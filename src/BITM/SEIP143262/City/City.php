<?php
namespace App\BITM\SEIP143262\City;
use App\BITM\SEIP143262\Message\Message;
use App\BITM\SEIP143262\Utility\Utility;
use App\BITM\SEIP143262\Model\Database as DB;
use PDO;

class City extends DB{
    public $id="";
    public $name="";
    public $city_name="";



    public function __construct(){
        parent::__construct();
    }


        public function index($fetchMode='ASSOC'){
            $fetchMode = strtoupper($fetchMode);
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from city where is_deleted='0'");
            $sth->execute();
            if(substr_count($fetchMode,'OBJ') > 0)
                $sth->setFetchMode(PDO::FETCH_OBJ);
            else
                $sth->setFetchMode(PDO::FETCH_ASSOC);

            $all_city=$sth->fetchAll();


            return  $all_city;
        }

    public function update(){
        $dbh=$this->connection;
        $values=array($this->name,$this->city_name);

        //var_dump($values);


        $query='UPDATE city SET  name = ?   , city_name = ? where id ='.$this->id;



        //    $query='UPDATE book_title  SET book_title  = ?   , author_name = ? where id ='.$this->id;

        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ name: $this->name ] ,
               [ City: $this->city_name ] <br> Data Has Been Updated Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Updated !</h3></div>");
        Utility::redirect('index.php');



    }
    public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from city Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }




    public function view($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from city Where id=$id");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $selected_city=$sth->fetch();

        return  $selected_city;

    }



    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('city_name',$data)){
            $this->city_name=$data['city_name'];

        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->name,$this->city_name);
        $query="insert into city(name,city_name) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        Message::message("<div id='msg'></div><h3 align='center'>[ name: $this->name ] ,
[  city : $this->city_name ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else
            Message::message("<div id='msg'></div><h3 align='center'>[ name: $this->name ] ,
[  city : $this->city_name ] <br> Data Hasn't Been Inserted Successfully!</h3></div>");



        Utility::redirect('create.php');



    }
    public function trash($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE city  SET is_deleted  = "1" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been trashed Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been trashed !</h3></div>");
        Utility::redirect('index.php');



    }



}

