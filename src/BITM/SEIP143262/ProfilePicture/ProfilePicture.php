<?php
namespace App\BITM\SEIP143262\ProfilePicture;
use App\BITM\SEIP143262\Message\Message;
use App\BITM\SEIP143262\Utility\Utility;
use App\BITM\SEIP143262\Model\Database as DB;
use PDO;

class ProfilePicture extends DB{
    public $id="";
    public $name="";
    public $picture_address="";



    public function __construct(){
        parent::__construct();
    }


    public function index($fetchMode='ASSOC'){
        $fetchMode = strtoupper($fetchMode);
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from profile_picture where is_deleted='0'");
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);

        $all_picture=$sth->fetchAll();


        return  $all_picture;
    }







    public function view($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from profile_picture Where id=$id");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $selected_person=$sth->fetch();

        return  $selected_person;

    }



    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('picture_address',$data)){
            $this->picture_address=$data['picture_address'];

        }
    }
    public function store(){
        $dbh=$this->connection;
        $values=array($this->name,$this->picture_address);
        $query="insert into profile_picture(name,picture_address) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ Name: $this->name ] ,
               [ profile picture: $this->picture_address ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('create.php');



    }


    public function update(){
        $dbh=$this->connection;
        $values=array($this->name,$this->picture_address);

        //var_dump($values);


        $query='UPDATE profile_picture SET  name = ?   , picture_address = ? where id ='.$this->id;



        //    $query='UPDATE book_title  SET book_title  = ?   , author_name = ? where id ='.$this->id;

        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
                <br> Data Has Been Updated Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Updated !</h3></div>");
        Utility::redirect('index.php');



    }
   public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from profile_picture Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }
    public function trash($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE profile_picture  SET is_deleted  = "1" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been trashed Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been trashed !</h3></div>");
        Utility::redirect('index.php');



    }


}

