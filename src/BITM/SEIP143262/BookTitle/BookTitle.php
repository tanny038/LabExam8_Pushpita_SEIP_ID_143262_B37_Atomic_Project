<?php
namespace App\BITM\SEIP143262\BookTitle;
use App\BITM\SEIP143262\Message\Message;
use App\BITM\SEIP143262\Utility\Utility;
use App\BITM\SEIP143262\Model\Database as DB;
use PDO;

class BookTitle extends DB{
    public $id="";
    public $book_title="";
    public $author_name="";



    public function __construct(){
        parent::__construct();
    }


        public function index($fetchMode='ASSOC'){
            $fetchMode = strtoupper($fetchMode);
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from book_title WHERE is_deleted='0'");
            $sth->execute();
            if(substr_count($fetchMode,'OBJ') > 0)
                $sth->setFetchMode(PDO::FETCH_OBJ);
            else
                $sth->setFetchMode(PDO::FETCH_ASSOC);


            $all_books=$sth->fetchAll();

            return  $all_books;
        }

        public function view($id){
            $DBH=$this->connection;
            $sth=$DBH->prepare("select * from book_title Where id=$id");
            $sth->execute();
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $selected_book=$sth->fetch();

            return  $selected_book;

        }



    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('book_title',$data)){
            $this->book_title=$data['book_title'];

        }
        if(array_key_exists('author_name',$data)){
            $this->author_name=$data['author_name'];

        }
    }
    public function store(){
       $dbh=$this->connection;
        $values=array($this->book_title,$this->author_name);
        $query="insert into book_title(book_title,author_name) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ BookTitle: $this->book_title ] ,
               [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('create.php');



    }
    public function update(){
        $dbh=$this->connection;
        $values=array($this->book_title,$this->author_name);

        //var_dump($values);


        $query='UPDATE book_title  SET book_title  = ?   , author_name = ? where id ='.$this->id;

        $sth=$dbh->prepare($query);
        $sth->execute($values);
      if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ BookTitle: $this->book_title ] ,
               [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');



    }
    public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from book_title Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }

    public function trash($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE book_title  SET is_deleted  = "1" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been trashed Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been trashed !</h3></div>");
        Utility::redirect('index.php');



    }

    public function trashed($fetchMode='ASSOC'){
        $fetchMode = strtoupper($fetchMode);
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from book_title WHERE is_deleted='1'");
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);


        $all_books=$sth->fetchAll();

        return  $all_books;
    }

    public function recover($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE book_title  SET is_deleted  = "0" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been recovered Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been recovered !</h3></div>");
        Utility::redirect('index.php');



    }


}

