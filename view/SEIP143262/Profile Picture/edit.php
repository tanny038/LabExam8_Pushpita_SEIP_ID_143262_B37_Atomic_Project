<?php
require_once("../../../vendor/autoload.php");

use App\BITM\SEIP143262\ProfilePicture\ProfilePicture;
use App\BITM\SEIP143262\Utility\Utility;


$obj= new ProfilePicture();
$id=$_GET['id'];



$selected_person= $obj->view($id);
//Utility::dd($selected_person);

?>

    <!DOCTYPE html>
    <html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/profile.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>


<div class="container">


    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Update Profile</h1>


                    </div>




                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-booktitle" method="Post" action="update.php" enctype="multipart/form-data">
                        <fieldset>
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <input class="form-control" value="<?php echo $selected_person['name'] ?>"  name="name" type="text">
                            <label ><h4>please choose a picture...</h4></label>

                            <input class="form-control"  name="fileToUpload" type="file">
                            <img  src="../../../resource/assets/profile_picture/<?php echo $selected_person['picture_address']?>" alt="image" height="250px" width="250px" class="img-responsive">
                            <input type="hidden" name="id" value="<?php echo $_GET['id']?>" >
                            <input type="hidden" name="address" value="<?php echo $selected_person['picture_address']?>" >
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Update »">
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>





</body>
</html>